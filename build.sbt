packageArchetype.java_application

organization  := "com.mvpotter"

name := "entertainment-finder"

version       := "1.0"

scalaVersion  := "2.11.6"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {
  val akkaVersion = "2.3.9"
  val sprayVersion = "1.3.3"
  val sprayJsonVersion = "1.3.1"
  val specs2Version = "2.3.11"
  Seq(
    "io.spray"            %%  "spray-can"     % sprayVersion,
    "io.spray"            %%  "spray-routing" % sprayVersion,
    "io.spray"            %%  "spray-client"  % sprayVersion,
    "io.spray"            %%  "spray-json"    % sprayJsonVersion,
    "io.spray"            %%  "spray-testkit" % sprayVersion      % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaVersion,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaVersion       % "test",
    "org.specs2"          %%  "specs2-core"   % specs2Version     % "test"
  )
}

Revolver.settings
