package com.mvpotter.dgis

import akka.actor.{Actor, Props}
import com.mvpotter.dgis.dgis.{DgisService, DgisServiceProtocol}
import DgisServiceProtocol.GetBestPlaces
import com.mvpotter.dgis.model.{ResultBranch, ApiJsonProtocol}
import org.specs2.mutable.Specification
import spray.httpx.SprayJsonSupport
import spray.routing.RequestContext
import spray.testkit.Specs2RouteTest
import spray.http._
import StatusCodes._

import SprayJsonSupport._
import ApiJsonProtocol._

trait TestData {
  def testBranch = ResultBranch("test name", "test address", Some(4.5))
}

class EntertainmentFinderServiceSpec extends Specification with Specs2RouteTest with TestEntertainmentFinderService with TestData {
  def actorRefFactory = system

  "EntertainmentFinderService" should {

    "return list of branches for GET requests to the /api/best path" in {
      Get("/api/best?category=restaurants") ~> apiRoute ~> check {
        status === OK
        responseAs[List[ResultBranch]] must be equalTo List(testBranch)
      }
    }

  }

}

class TestDgisServiceActor(requestContext: RequestContext) extends Actor with TestData {

  override def receive: Receive = {
    case GetBestPlaces(cities, category) =>
      requestContext.complete(List(testBranch))
      context.stop(self)
  }

}

trait TestDgisService extends DgisService {
  override def dgisProps(requestContext: RequestContext) = Props(classOf[TestDgisServiceActor], requestContext)
}

trait TestEntertainmentFinderService extends EntertainmentFinderService with TestDgisService
