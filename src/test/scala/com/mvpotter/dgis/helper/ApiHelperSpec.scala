package com.mvpotter.dgis.helper

import com.mvpotter.dgis.hepler.ApiHelper
import com.mvpotter.dgis.model.DgisApiListResponse
import org.specs2.mutable.Specification
import spray.httpx.UnsuccessfulResponseException

class ApiHelperSpec extends Specification{

  "checkResponse" should {
    "do nothing on success response code" in {
      ApiHelper.checkResponse(new DgisApiListResponse(response_code = "200"))
      1 mustEqual 1
    }
    "throw UnsuccessfulResponseException on failure response code" in {
      ApiHelper.checkResponse(new DgisApiListResponse(response_code = "401")) should throwA[UnsuccessfulResponseException]
    }
  }

  "appendAddress" should {
    "add address to a city if provided" in {
      ApiHelper.appendAddress("city", Some("address")) mustEqual "city, address"
    }
    "return city if address is none" in {
      ApiHelper.appendAddress("city", None) mustEqual "city"
    }
  }

}
