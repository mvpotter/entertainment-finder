package com.mvpotter.dgis.config

import com.typesafe.config.ConfigFactory

trait Parameters {

  val config = ConfigFactory.load()
  val dgisConfig = config.getConfig("dgis")

}
