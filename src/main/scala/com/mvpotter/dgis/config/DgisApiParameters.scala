package com.mvpotter.dgis.config

import spray.http.Uri.Query

trait DgisApiParameters extends Parameters {

  def apiBaseUrl = dgisConfig.getString("api.baseUrl")
  def apiVersion = dgisConfig.getString("api.version")
  def apiKey = dgisConfig.getString("api.key")
  def baseQuery = Query("key" -> apiKey, "version" -> apiVersion)

}
