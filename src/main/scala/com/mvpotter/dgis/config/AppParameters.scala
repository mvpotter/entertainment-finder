package com.mvpotter.dgis.config

import scala.collection.JavaConversions._

trait AppParameters extends Parameters{

  def cities = dgisConfig.getStringList("cities").toList

}
