package com.mvpotter.dgis

import akka.actor.Actor
import com.mvpotter.dgis.config.AppParameters
import com.mvpotter.dgis.dgis.{ProdDgisService, DgisServiceProtocol, DgisService}
import com.mvpotter.dgis.model.{ApiJsonProtocol, Error}
import spray.httpx.{SprayJsonSupport, UnsuccessfulResponseException}
import spray.routing._
import spray.http._
import MediaTypes._
import spray.util.LoggingContext

import SprayJsonSupport._
import ApiJsonProtocol._

class EntertainmentFinderServiceActor extends Actor with ProdEntertainmentFinderService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = handleTimeouts orElse runRoute(apiRoute)

  import spray.json._
  def handleTimeouts: Receive = {
    case Timedout(request: HttpRequest) =>
      val errorStatus = StatusCodes.InternalServerError
      sender() ! HttpResponse(errorStatus, HttpEntity(`application/json`, Error(errorStatus.intValue, "Timeout").toJson.toString()))
  }
}

trait EntertainmentFinderService extends HttpService with AppParameters with DgisService {

  implicit def routeExceptionHandler(implicit log: LoggingContext): ExceptionHandler = ExceptionHandler {
    case e: UnsuccessfulResponseException =>
      requestUri { uri =>
        complete(e.response.status, Error(e.response.status.intValue, e.response.entity.asString))
      }
  }

  val apiRoute =
    pathPrefix("api") {
      path("best") {
        get {
          parameters('category) { category =>
            findBestPlaces(category)
          }
        }
      }
    }

    import DgisServiceProtocol._

    def findBestPlaces(category: String) = { requestContext: RequestContext =>
      val dgisService = actorRefFactory.actorOf(dgisProps(requestContext))
      dgisService ! GetBestPlaces(cities, category)
    }

}

trait ProdEntertainmentFinderService extends EntertainmentFinderService with ProdDgisService