package com.mvpotter.dgis.dgis

import akka.actor.{Actor, ActorLogging}
import com.mvpotter.dgis.hepler.ApiHelper
import com.mvpotter.dgis.model._
import spray.http.{HttpEntity, HttpResponse, StatusCode}
import spray.httpx.{SprayJsonSupport, UnsuccessfulResponseException}
import spray.routing.RequestContext

import scala.concurrent.Future
import scala.util.{Failure, Success}

object DgisServiceProtocol {
  case class GetBestPlaces(cities: List[String], category: String)
}

abstract class DgisServiceActor(requestContext: RequestContext) extends Actor with ActorLogging with DgisClient {

  import com.mvpotter.dgis.dgis.DgisServiceProtocol._
  import SprayJsonSupport._
  import ApiJsonProtocol._
  import ApiHelper._

  implicit val system = context.system
  import system.dispatcher

  override def receive: Receive = {
    case GetBestPlaces(cities, category) =>
      process(cities, category)
      context.stop(self)
  }

  def process(cities: List[String], category: String) = {

    log.info(s"Requesting popular branches for $category category")

    val branchesRequest = Future.sequence(cities.map { city =>
      for {
        branch <- getTopBranch(getBranches(category, city))
        profile <- toResultBranch(getProfile(branch))
      } yield profile
    })
    branchesRequest onComplete {
      case Success(branches) =>
        requestContext.complete(branches.sortWith((b1, b2) => b1.flamp_rate.getOrElse(0.0) > b2.flamp_rate.getOrElse(0.0)))
      case Failure(error) =>
        requestContext.complete(error)
    }
  }

}

