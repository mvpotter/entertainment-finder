package com.mvpotter.dgis.dgis

import akka.actor.ActorRefFactory
import com.mvpotter.dgis.config.DgisApiParameters
import com.mvpotter.dgis.model._
import spray.client.pipelining._
import spray.http.Uri.Query
import spray.http._
import spray.httpx.SprayJsonSupport

import scala.concurrent.{ExecutionContext, Future}

trait DgisClient {
  def getBranches(what: String, where: String): Future[DgisApiListResponse[Branch]]
  def getProfile(branch: Branch): Future[DgisApiProfileResponse]
}

trait ProdDgisClient extends DgisClient with DgisApiParameters {

  implicit val system: ActorRefFactory
  implicit def dispatcher: ExecutionContext

  import SprayJsonSupport._
  import DgisApiJsonProtocol._

  def apiSearchUrl = s"$apiBaseUrl/search"
  def apiProfileUrl = s"$apiBaseUrl/profile"

  override def getBranches(what: String, where: String): Future[DgisApiListResponse[Branch]] = {
    val pipeline = sendReceive ~> unmarshal[DgisApiListResponse[Branch]]
    val query: Query = baseQuery.+:("what", what).+:("where", where).+:("sort", "rating")
    val searchUri = Get(Uri(apiSearchUrl).withQuery(query))
    pipeline(searchUri)
  }

  override def getProfile(branch: Branch): Future[DgisApiProfileResponse] = {
    val pipeline = sendReceive ~> unmarshal[DgisApiProfileResponse]
    val query: Query = baseQuery.+:("id", branch.id).+:("hash", branch.hash)
    val profileUri = Get(Uri(apiProfileUrl).withQuery(query))
    pipeline(profileUri)
  }

}


