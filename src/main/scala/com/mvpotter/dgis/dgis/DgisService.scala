package com.mvpotter.dgis.dgis

import akka.actor.Props
import spray.routing.RequestContext

trait DgisService {
  def dgisProps(requestContext: RequestContext): Props
}

trait ProdDgisService extends DgisService {
  override def dgisProps(requestContext: RequestContext) = Props(classOf[ProdDgisServiceActor], requestContext)
}

/**
 * Responsible for 2GIS API interaction.
 *
 * @param requestContext request context
 */
class ProdDgisServiceActor(requestContext: RequestContext) extends DgisServiceActor(requestContext) with ProdDgisClient {
  override val system = context.system
  override def dispatcher = system.dispatcher
}

