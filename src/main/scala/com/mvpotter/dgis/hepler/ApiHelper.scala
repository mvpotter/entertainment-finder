package com.mvpotter.dgis.hepler

import com.mvpotter.dgis.model._
import spray.http.{HttpEntity, HttpResponse, StatusCode}
import spray.httpx.UnsuccessfulResponseException

import scala.concurrent.{ExecutionContext, Future}

object ApiHelper {

  def getTopBranch(branchesResponse: Future[DgisApiListResponse[Branch]])(implicit ec: ExecutionContext): Future[Branch] = {
    branchesResponse.map { response =>
      checkResponse(response)
      response.result.get.head
    }
  }

  def toResultBranch(profileResponse: Future[DgisApiProfileResponse])(implicit ec: ExecutionContext): Future[ResultBranch] = {
    profileResponse.map(response => {
      checkResponse(response)
      ResultBranch(name = response.name, address = appendAddress(response.city_name, response.address),
        flamp_rate = response.rating.flatMap(rating => rating.toDouble match {
          case 0 => None
          case value => Some(value)
        }))
    })
  }

  def checkResponse(response: DgisApiResponse): Unit = {
    val status = StatusCode.int2StatusCode(response.response_code.toInt)
    if (status.isFailure) {
      throw new UnsuccessfulResponseException(HttpResponse(status, HttpEntity(response.error_message.getOrElse("Unknown error"))))
    }
  }

  def appendAddress(city: String, address: Option[String]): String = {
    address match {
      case Some(addr) => s"$city, $addr"
      case None => city
    }
  }


}
