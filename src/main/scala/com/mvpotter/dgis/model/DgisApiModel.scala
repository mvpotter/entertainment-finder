package com.mvpotter.dgis.model

import spray.json.{RootJsonFormat, DefaultJsonProtocol, JsonFormat}

case class Branch(id: String, hash: String, name: String, city_name: String, address: String)

trait DgisApiResponse {
  def response_code: String
  def error_code: Option[String] = None
  def error_message: Option[String] = None
}

case class DgisApiListResponse[T](override val response_code: String,
                                  override val error_code: Option[String] = None,
                                  override val error_message: Option[String] = None,
                                  result: Option[List[T]] = None) extends DgisApiResponse

case class DgisApiProfileResponse (override val response_code: String,
                                   override val error_code: Option[String] = None,
                                   override val error_message: Option[String] = None,
                                   id: String, name: String, city_name: String, address: Option[String],
                                   rating: Option[String]) extends DgisApiResponse

object DgisApiJsonProtocol extends DefaultJsonProtocol {
  implicit val branchFormat = jsonFormat5(Branch)
  implicit def dgisApiListResponseFormat[T :JsonFormat]: RootJsonFormat[DgisApiListResponse[T]] = jsonFormat4(DgisApiListResponse.apply[T])
  implicit def dgisApiProfileResponseFormat: RootJsonFormat[DgisApiProfileResponse] = jsonFormat8(DgisApiProfileResponse)
}