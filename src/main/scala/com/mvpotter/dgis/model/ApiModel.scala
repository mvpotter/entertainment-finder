package com.mvpotter.dgis.model

import spray.json.DefaultJsonProtocol

case class ResultBranch(name: String, address: String, flamp_rate: Option[Double])
case class Error(status: Int, error: String)

object ApiJsonProtocol extends DefaultJsonProtocol {
  implicit val errorFormat = jsonFormat2(Error)
  implicit val resultBranchFormat = jsonFormat3(ResultBranch)
}