package com.mvpotter.dgis

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.util.Properties

object Boot extends App {

  implicit val system = ActorSystem("dgis-service")
  val service = system.actorOf(Props[EntertainmentFinderServiceActor], "entertainment-finder")
  implicit val timeout = Timeout(5.seconds)
  val port = Properties.envOrElse("PORT", "8080").toInt
  // start a new HTTP server on port 8080 with our service actor as the handler
  IO(Http) ? Http.Bind(service, interface = "0.0.0.0", port = port)

}
